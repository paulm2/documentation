# Dutch translations for Inkscape.
# This file is distributed under the same license as the Inkscape package.
#
# Kris De Gussem <kris.DeGussem@gmail.com>, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Tracing\n"
"POT-Creation-Date: 2020-04-28 20:51+0200\n"
"PO-Revision-Date: 2010-11-06 09:25+0100\n"
"Last-Translator: Kris De Gussem <Kris.DeGussem@gmail.com>\n"
"Language-Team: Dutch <vertaling@vrijschrift.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Dutch\n"
"X-Poedit-Country: NETHERLANDS\n"

#: tracing-f06.svg:49(format) tracing-f05.svg:49(format)
#: tracing-f04.svg:49(format) tracing-f03.svg:49(format)
#: tracing-f02.svg:49(format) tracing-f01.svg:49(format)
msgid "image/svg+xml"
msgstr "image/svg+xml"

#: tracing-f06.svg:70(tspan) tracing-f05.svg:70(tspan)
#: tracing-f04.svg:70(tspan) tracing-f03.svg:70(tspan)
#: tracing-f02.svg:70(tspan)
#, no-wrap
msgid "Original Image"
msgstr "Originele afbeelding"

#: tracing-f06.svg:81(tspan)
#, no-wrap
msgid "Traced Image / Output Path - Simplified"
msgstr "Overtrokken afbeelding / uitvoer - vereenvoudigd"

#: tracing-f06.svg:86(tspan)
#, no-wrap
msgid "(384 nodes)"
msgstr "(384 knooppunten)"

#: tracing-f05.svg:81(tspan)
#, no-wrap
msgid "Traced Image / Output Path"
msgstr "Overtrokken afbeelding / uitvoer"

#: tracing-f05.svg:86(tspan)
#, no-wrap
msgid "(1,551 nodes)"
msgstr "(1551 knooppunten)"

#: tracing-f04.svg:81(tspan) tracing-f04.svg:97(tspan)
#, no-wrap
msgid "Quantization (12 colors)"
msgstr "Quantisatie (12 kleuren)"

#: tracing-f04.svg:86(tspan) tracing-f03.svg:86(tspan)
#: tracing-f02.svg:86(tspan)
#, no-wrap
msgid "Fill, no Stroke"
msgstr "Vulling, geen lijn"

#: tracing-f04.svg:102(tspan) tracing-f03.svg:102(tspan)
#: tracing-f02.svg:102(tspan)
#, no-wrap
msgid "Stroke, no Fill"
msgstr "Lijn, geen vulling"

#: tracing-f03.svg:81(tspan) tracing-f03.svg:97(tspan)
#, no-wrap
msgid "Edge Detected"
msgstr "Rand gedetecteerd"

#: tracing-f02.svg:81(tspan) tracing-f02.svg:97(tspan)
#, no-wrap
msgid "Brightness Threshold"
msgstr "Helderheid grenswaarde"

#: tracing-f01.svg:70(tspan)
#, no-wrap
msgid "Main options within the Trace dialog"
msgstr "Algemene opties in het dialoogvenster Bitmap overtrekken"

#: tutorial-tracing.xml:7(title)
#, fuzzy
msgid "Tracing bitmaps"
msgstr "Overtrekken"

#: tutorial-tracing.xml:8(subtitle)
msgid "Tutorial"
msgstr ""

#: tutorial-tracing.xml:13(para)
msgid ""
"One of the features in Inkscape is a tool for tracing a bitmap image into a "
"&lt;path&gt; element for your SVG drawing. These short notes should help you "
"become acquainted with how it works."
msgstr ""
"Een van de functies in Inkscape is een gereedschap voor het overtrekken van "
"bitmapafbeeldingen in een &lt;path&gt; element voor je SVG-afbeelding. Deze "
"korte nota's helpen je mee op weg."

#: tutorial-tracing.xml:20(para)
msgid ""
"Currently Inkscape employs the Potrace bitmap tracing engine (<ulink url="
"\"http://potrace.sourceforge.net\">potrace.sourceforge.net</ulink>) by Peter "
"Selinger. In the future we expect to allow alternate tracing programs; for "
"now, however, this fine tool is more than sufficient for our needs."
msgstr ""
"Op dit moment gebruikt Inkscape de Potrace engine van Peter Selinger voor "
"het overtrekken van bitmaps (<ulink url=\"http://potrace.sourceforge.net"
"\">potrace.sourceforge.net</ulink>). In de toekomst gaan we mogelijk "
"alternatieve overtrekprogramma's ondersteunen. Echter, nu is deze goede tool "
"meer dan voldoende voor onze noden."

#: tutorial-tracing.xml:27(para)
msgid ""
"Keep in mind that the Tracer's purpose is not to reproduce an exact "
"duplicate of the original image; nor is it intended to produce a final "
"product. No autotracer can do that. What it does is give you a set of curves "
"which you can use as a resource for your drawing."
msgstr ""
"Hou in het achterhoofd dat het doel van overtrekken noch het reproduceren is "
"van een exacte kopie van het origineel, noch bedoeld is voor het maken van "
"een finale afbeelding. Geen enkel overtrekprogramma kan dat. Het geeft je "
"echter een set curves die je als bron voor je afbeelding kan gebruiken."

#: tutorial-tracing.xml:34(para)
msgid ""
"Potrace interprets a black and white bitmap, and produces a set of curves. "
"For Potrace, we currently have three types of input filters to convert from "
"the raw image to something that Potrace can use."
msgstr ""
"Potrace interpreteert een zwart-witafbeelding en maakt hiervan een set "
"curves. We hebben nu drie types invoerfilters voor Potrace om de ruwe "
"afbeelding te converteren in iets dat Potrace kan gebruiken."

#: tutorial-tracing.xml:40(para)
msgid ""
"Generally the more dark pixels in the intermediate bitmap, the more tracing "
"that Potrace will perform. As the amount of tracing increases, more CPU time "
"will be required, and the &lt;path&gt; element will become much larger. It "
"is suggested that the user experiment with lighter intermediate images "
"first, getting gradually darker to get the desired proportion and complexity "
"of the output path."
msgstr ""
"Algemeen geldt dat hoe meer donkere pixels in de intermediaire bitmap, hoe "
"meer Potrace zal overtrekken. Omdat de hoeveelheid overtrek vergroot, is "
"meer CPU-tijd nodig en zal het &lt;path&gt;-element veel groter worden. Het "
"is een goed idee dat je als gebruiker eerst experimenteert met lichtere "
"afbeeldingen en dan langzaam overgaat naar donkere afbeeldingen om de "
"gewenste proportie en complexiteit van het uitvoerpad te verkrijgen."

#: tutorial-tracing.xml:48(para)
#, fuzzy
msgid ""
"To use the tracer, load or import an image, select it, and select the "
"<menuchoice><guimenu>Path</guimenu><guimenuitem>Trace Bitmap</guimenuitem></"
"menuchoice> item, or <keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"alt\">Alt</keycap><keycap>B</keycap></keycombo>."
msgstr ""
"Om de tracer te gebruiken, laad of importeer je een afbeelding, selecteer "
"deze en klik op het commando <command>Paden &gt; Bitmap overtrekken</"
"command> of gebruik je <keycap>Shift+Alt+B</keycap>."

#: tutorial-tracing.xml:60(para)
msgid "The user will see the three filter options available:"
msgstr "De gebruiker zal de volgende drie beschikbare filteropties zien:"

#: tutorial-tracing.xml:65(para)
#, fuzzy
msgid "Brightness Cutoff"
msgstr "Helderheid grenswaarde"

#: tutorial-tracing.xml:70(para)
msgid ""
"This merely uses the sum of the red, green and blue (or shades of gray) of a "
"pixel as an indicator of whether it should be considered black or white. The "
"threshold can be set from 0.0 (black) to 1.0 (white). The higher the "
"threshold setting, the fewer the number pixels that will be considered to be "
"“white”, and the intermediate image with become darker."
msgstr ""
"Deze optie gebruikt in essentie de som van de rood-, groen- en blauwwaarde "
"(of grijstint) van een pixel als een zwart-witindicator. De grenswaarde kan "
"ingesteld worden van 0.0 (zwart) tot 1.0 (wit). Hoe hoger de grenswaarde, "
"hoe kleiner het aantal pixels dat als “wit” wordt gezien en bijgevolg hoe "
"donkerder de intermediaire afbeelding."

#: tutorial-tracing.xml:86(para)
#, fuzzy
msgid "Edge Detection"
msgstr "Rand gedetecteerd"

#: tutorial-tracing.xml:91(para)
msgid ""
"This uses the edge detection algorithm devised by J. Canny as a way of "
"quickly finding isoclines of similar contrast. This will produce an "
"intermediate bitmap that will look less like the original image than does "
"the result of Brightness Threshold, but will likely provide curve "
"information that would otherwise be ignored. The threshold setting here (0.0 "
"– 1.0) adjusts the brightness threshold of whether a pixel adjacent to a "
"contrast edge will be included in the output. This setting can adjust the "
"darkness or thickness of the edge in the output."
msgstr ""
"Deze optie gebruikt het randdetectiealgoritme van J. Canny als een snelle "
"methode voor het vinden van lijnen met dezelfde kleurgradiënt of contrast. "
"Dit zal een intermediaire afbeelding produceren die minder lijkt op de "
"originele tekening dan met de optie Helderheid, maar zal wellicht curve-"
"informatie bevatten die anders genegeerd wordt. De grenswaarde (0.0 - 1.0) "
"past hier de grenswaarde voor de helderheid aan die bepaalt of een pixel "
"langs de contrastrand in de uitvoer terug te vinden is. Deze instelling kan "
"de donkerheid of randdikte in de uitvoer aanpassen."

#: tutorial-tracing.xml:109(para)
msgid "Color Quantization"
msgstr "Kleurquantisatie"

#: tutorial-tracing.xml:114(para)
msgid ""
"The result of this filter will produce an intermediate image that is very "
"different from the other two, but is very useful indeed. Instead of showing "
"isoclines of brightness or contrast, this will find edges where colors "
"change, even at equal brightness and contrast. The setting here, Number of "
"Colors, decides how many output colors there would be if the intermediate "
"bitmap were in color. It then decides black/white on whether the color has "
"an even or odd index."
msgstr ""
"Het resultaat van deze filter produceert een intermediaire afbeelding die "
"zeer verschillend is van de twee andere, maar niettemin erg bruikbaar is. In "
"tegenstelling met het tonen van lijnen met gelijke helderheid of contrast, "
"zal deze filter randen vinden waar kleuren veranderen, zelfs bij gelijke "
"helderheid en contrast. De instelling, aantal kleuren, bepaalt hoeveel "
"kleuren er in de intermediaire afbeelding zouden zijn, indien deze "
"intermediaire bitmap in kleur was. Het kent dan zwart en wit toe afhankelijk "
"van het feit of de kleur een even of oneven index heeft."

#: tutorial-tracing.xml:130(para)
msgid ""
"The user should try all three filters, and observe the different types of "
"output for different types of input images. There will always be an image "
"where one works better than the others."
msgstr ""
"De gebruiker zou de drie filters moeten proberen en de verschillende types "
"uitvoer voor verschillende types afbeeldingen observeren. Er is altijd een "
"afbeelding waar de ene filter beter werkt dan de andere."

#: tutorial-tracing.xml:136(para)
#, fuzzy
msgid ""
"After tracing, it is also suggested that the user try "
"<menuchoice><guimenu>Path</guimenu><guimenuitem>Simplify</guimenuitem></"
"menuchoice> (<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>L</"
"keycap></keycombo>) on the output path to reduce the number of nodes. This "
"can make the output of Potrace much easier to edit. For example, here is a "
"typical tracing of the Old Man Playing Guitar:"
msgstr ""
"Na het overtrekken is het aan te raden om het commando <command>Paden &gt; "
"Vereenvoudigen</command> (<keycap>Ctrl+L</keycap>) toe te passen op het "
"uitvoerpad om het aantal knooppunten te reduceren. Dit vereenvoudigt het "
"bewerken van de potrace-uitvoer. Hier is bijvoorbeeld een typische overtrek "
"van de Oude gitaarspeler:"

#: tutorial-tracing.xml:150(para)
#, fuzzy
msgid ""
"Note the enormous number of nodes in the path. After hitting "
"<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>L</keycap></"
"keycombo>, this is a typical result:"
msgstr ""
"Zie het enorm aantal knooppunten in het pad. Na het drukken op <keycap>Ctrl"
"+L</keycap> is dit een typisch resultaat:"

#: tutorial-tracing.xml:162(para)
msgid ""
"The representation is a bit more approximate and rough, but the drawing is "
"much simpler and easier to edit. Keep in mind that what you want is not an "
"exact rendering of the image, but a set of curves that you can use in your "
"drawing."
msgstr ""
"De voorstelling is een iets benaderender en ruwer, maar de afbeelding is "
"veel eenvoudiger en bewerkbaarder. Hou in het achterhoofd dat wat je wil "
"niet een exacte rendering is van de afbeelding, maar een set curves die je "
"kan gebruiken in je afbeelding."

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-tracing.xml:0(None)
msgid "translator-credits"
msgstr "Kris De Gussem <kris.DeGussem@gmail.com>, 2010"

#~ msgid "Optimal Edge Detection"
#~ msgstr "Optimale randdetectie"
