msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-04-28 20:51+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: elements-f15.svg:452(format) elements-f14.svg:173(format)
#: elements-f13.svg:102(format) elements-f12.svg:445(format)
#: elements-f11.svg:48(format) elements-f10.svg:48(format)
#: elements-f09.svg:48(format) elements-f08.svg:206(format)
#: elements-f07.svg:91(format) elements-f06.svg:91(format)
#: elements-f05.svg:48(format) elements-f04.svg:48(format)
#: elements-f03.svg:48(format) elements-f02.svg:48(format)
#: elements-f01.svg:48(format)
msgid "image/svg+xml"
msgstr ""

#: elements-f12.svg:710(tspan)
#, no-wrap
msgid "Random Ant &amp; 4WD"
msgstr ""

#: elements-f12.svg:721(tspan)
#, no-wrap
msgid "SVG  Image Created by Andrew Fitzsimon"
msgstr ""

#: elements-f12.svg:726(tspan)
#, no-wrap
msgid "Courtesy of Open Clip Art Library"
msgstr ""

#: elements-f12.svg:731(tspan)
#, no-wrap
msgid "http://www.openclipart.org/"
msgstr ""

#: elements-f04.svg:79(tspan)
#, no-wrap
msgid "BIG"
msgstr ""

#: elements-f04.svg:93(tspan)
#, no-wrap
msgid "small"
msgstr ""

#: elements-f01.svg:88(tspan)
#, no-wrap
msgid "Elements"
msgstr ""

#: elements-f01.svg:101(tspan)
#, no-wrap
msgid "Principles"
msgstr ""

#: elements-f01.svg:114(tspan) tutorial-elements.xml:96(title)
#, no-wrap
msgid "Color"
msgstr ""

#: elements-f01.svg:127(tspan) tutorial-elements.xml:33(title)
#, no-wrap
msgid "Line"
msgstr ""

#: elements-f01.svg:140(tspan) tutorial-elements.xml:49(title)
#, no-wrap
msgid "Shape"
msgstr ""

#: elements-f01.svg:153(tspan) tutorial-elements.xml:79(title)
#, no-wrap
msgid "Space"
msgstr ""

#: elements-f01.svg:166(tspan) tutorial-elements.xml:113(title)
#, no-wrap
msgid "Texture"
msgstr ""

#: elements-f01.svg:179(tspan) tutorial-elements.xml:128(title)
#, no-wrap
msgid "Value"
msgstr ""

#: elements-f01.svg:192(tspan) tutorial-elements.xml:65(title)
#, no-wrap
msgid "Size"
msgstr ""

#: elements-f01.svg:205(tspan) tutorial-elements.xml:150(title)
#, no-wrap
msgid "Balance"
msgstr ""

#: elements-f01.svg:218(tspan) tutorial-elements.xml:166(title)
#, no-wrap
msgid "Contrast"
msgstr ""

#: elements-f01.svg:231(tspan) tutorial-elements.xml:179(title)
#, no-wrap
msgid "Emphasis"
msgstr ""

#: elements-f01.svg:244(tspan) tutorial-elements.xml:194(title)
#, no-wrap
msgid "Proportion"
msgstr ""

#: elements-f01.svg:257(tspan) tutorial-elements.xml:208(title)
#, no-wrap
msgid "Pattern"
msgstr ""

#: elements-f01.svg:271(tspan) tutorial-elements.xml:222(title)
#, no-wrap
msgid "Gradation"
msgstr ""

#: elements-f01.svg:284(tspan) tutorial-elements.xml:240(title)
#, no-wrap
msgid "Composition"
msgstr ""

#: elements-f01.svg:297(tspan)
#, no-wrap
msgid "Overview"
msgstr ""

#: tutorial-elements.xml:7(title)
msgid "Elements of design"
msgstr ""

#: tutorial-elements.xml:8(subtitle)
msgid "Tutorial"
msgstr ""

#: tutorial-elements.xml:13(para)
msgid ""
"This tutorial will demonstrate the elements and principles of design which "
"are normally taught to early art students in order to understand various "
"properties used in art making. This is not an exhaustive list, so please "
"add, subtract, and combine to make this tutorial more comprehensive."
msgstr ""

#: tutorial-elements.xml:29(title)
msgid "Elements of Design"
msgstr ""

#: tutorial-elements.xml:30(para)
msgid "The following elements are the building blocks of design."
msgstr ""

#: tutorial-elements.xml:34(para)
msgid ""
"A line is defined as a mark with length and direction, created by a point "
"that moves across a surface. A line can vary in length, width, direction, "
"curvature, and color. Line can be two-dimensional (a pencil line on paper), "
"or implied three-dimensional."
msgstr ""

#: tutorial-elements.xml:50(para)
msgid ""
"A flat figure, shape is created when actual or implied lines meet to "
"surround a space. A change in color or shading can define a shape. Shapes "
"can be divided into several types: geometric (square, triangle, circle) and "
"organic (irregular in outline)."
msgstr ""

#: tutorial-elements.xml:66(para)
msgid ""
"This refers to variations in the proportions of objects, lines or shapes. "
"There is a variation of sizes in objects either real or imagined."
msgstr ""

#: tutorial-elements.xml:80(para)
msgid ""
"Space is the empty or open area between, around, above, below, or within "
"objects. Shapes and forms are made by the space around and within them. "
"Space is often called three-dimensional or two- dimensional. Positive space "
"is filled by a shape or form. Negative space surrounds a shape or form."
msgstr ""

#: tutorial-elements.xml:97(para)
msgid ""
"Color is the perceived character of a surface according to the wavelength of "
"light reflected from it. Color has three dimensions: HUE (another word for "
"color, indicated by its name such as red or yellow), VALUE (its lightness or "
"darkness), INTENSITY (its brightness or dullness)."
msgstr ""

#: tutorial-elements.xml:114(para)
msgid ""
"Texture is the way a surface feels (actual texture) or how it may look "
"(implied texture). Textures are described by word such as rough, silky, or "
"pebbly."
msgstr ""

#: tutorial-elements.xml:129(para)
msgid ""
"Value is how dark or how light something looks. We achieve value changes in "
"color by adding black or white to the color. Chiaroscuro uses value in "
"drawing by dramatically contrasting lights and darks in a composition."
msgstr ""

#: tutorial-elements.xml:146(title)
msgid "Principles of Design"
msgstr ""

#: tutorial-elements.xml:147(para)
msgid "The principles use the elements of design to create a composition."
msgstr ""

#: tutorial-elements.xml:151(para)
msgid ""
"Balance is a feeling of visual equality in shape, form, value, color, etc. "
"Balance can be symmetrical or evenly balanced or asymmetrical and un-evenly "
"balanced. Objects, values, colors, textures, shapes, forms, etc., can be "
"used in creating a balance in a composition."
msgstr ""

#: tutorial-elements.xml:167(para)
msgid "Contrast is the juxtaposition of opposing elements"
msgstr ""

#: tutorial-elements.xml:180(para)
msgid ""
"Emphasis is used to make certain parts of their artwork stand out and grab "
"your attention. The center of interest or focal point is the place a work "
"draws your eye to first."
msgstr ""

#: tutorial-elements.xml:195(para)
msgid ""
"Proportion describes the size, location or amount of one thing compared to "
"another."
msgstr ""

#: tutorial-elements.xml:209(para)
msgid ""
"Pattern is created by repeating an element (line, shape or color) over and "
"over again."
msgstr ""

#: tutorial-elements.xml:223(para)
msgid ""
"Gradation of size and direction produce linear perspective. Gradation of "
"color from warm to cool and tone from dark to light produce aerial "
"perspective. Gradation can add interest and movement to a shape. A gradation "
"from dark to light will cause the eye to move along a shape."
msgstr ""

#: tutorial-elements.xml:241(para)
msgid "The combining of distinct elements to form a whole."
msgstr ""

#: tutorial-elements.xml:251(title)
msgid "Bibliography"
msgstr ""

#: tutorial-elements.xml:252(para)
msgid "This is a partial bibliography used to build this document."
msgstr ""

#: tutorial-elements.xml:255(ulink)
msgid "http://www.makart.com/resources/artclass/EPlist.html"
msgstr ""

#: tutorial-elements.xml:258(ulink)
msgid "http://www.princetonol.com/groups/iad/Files/elements2.htm"
msgstr ""

#: tutorial-elements.xml:261(ulink)
msgid "http://www.johnlovett.com/test.htm"
msgstr ""

#: tutorial-elements.xml:265(ulink)
msgid "http://digital-web.com/articles/elements_of_design/"
msgstr ""

#: tutorial-elements.xml:269(ulink)
msgid "http://digital-web.com/articles/principles_of_design/"
msgstr ""

#: tutorial-elements.xml:274(para)
msgid ""
"Special thanks to Linda Kim (<ulink url=\"http://www.coroflot.com/redlucite/"
"\">http://www.coroflot.com/redlucite/</ulink>) for helping me (<ulink url="
"\"http://www.rejon.org/\">http://www.rejon.org/</ulink>) with this tutorial. "
"Also, thanks to the Open Clip Art Library (<ulink url=\"http://www."
"openclipart.org/\">http://www.openclipart.org/</ulink>) and the graphics "
"people have submitted to that project."
msgstr ""

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-elements.xml:0(None)
msgid "translator-credits"
msgstr ""
